#!/bin/bash

#Standard library implementation, contains repeatable code, stored in exportable functions, to create a shakeable code-tree.
#This standard prevents redundancy, improves efficiency, and makes adapting the codebase much easier.

#Import dependencies
source library/lib_filetypes.bash
source library/lib_textformat.bash

function check_filesystem() {
 clear

#Note that big prints like this can cause problems with some fonts; Switch to a standard font when making these.

 if [ ! -f "$exe_binary" ] && [ ! -f "$dll_binary" ] && [ ! -f DyingLightGame ] ; then #`DyingLightGame` is the Linux binary. Ensure they are not using Linux native.
	printf "┌────────────────────────────────<¡Error!>────────────────────────────────┐\n"
	printf "│ This does not appear to be the /Dying Light/ folder. Please check that  │\n"
	printf "│ you installed this script to the /Dying Light/ folder.                  │\n"
	printf "│─────────────────────────────────────────────────────────────────────────│\n"
	printf "│ 1. Right click ${bold}Dying Light${normal} in your Steam library.                       │\n" #These render fine as the tags do not render; They only modify the state of the text they affect.
	printf "│                                                                         │\n"
	printf "│ 2. Navigate to ${bold}Manage -> Browse${normal} Local Files                             │\n"
	printf "│                                                                         │\n"
	printf "│ 3. Place this script into the folder.                                   │\n"
	printf "│─────────────────────────────────────────────────────────────────────────│\n"
	printf "│ Report problems or questions/suggestions here:                          │\n"
	printf "│ ${bold}https://gitlab.com/dlimi/dlimcsm/-/issues/new${normal}                           │\n"
	printf "└─────────────────────────────────────────────────────────────────────────┘\n"
 fi
}
