<!--Template dropdown section
<details>
<summary><h1></h1></summary>

#
</details>-->

<!--CRITERIA FOR DOCUMENTATION FRIENDLINESS-->
<!--1: Assume no prerequisite knowledge. Of computing or in general Operating System use.-->
<!--2: Use emoji's and Markdown to adequately articulate and format information.-->
<!--3: Use no indentation unless necessary for Markdown. To avoid issues with format, size, and subsection nest limitation.-->
<!--NOTE: Use https://www.emojicopy.com to get Emoji's.-->

# 🎇 Welcome! Choose a language below, by clicking on its header. 🎉

<details><!--English-->
<summary><h2>🇺🇸 🇬🇧 🌲 English 🌽 🇦🇺 🇳🇿</h2></summary>

## 🍃 Dying Light Improved Compatibility Support Module (*`dlimcsm`*) 🛠️
Multi-player compatible [`dlim`](https://gitlab.com/dlimi/dlim)-based efficiency and performance enhancement to [**Dying Light**](https://dl1.dyinglightgame.com).
#

<details><!--Downloads-->
<summary><h2>📩 Downloads 📬</h2></summary>

A simple download page, for those who know how to install.

The platform distinction is about [`DLDXVK`](https://gitlab.com/dlimi/dldxvk):

> Using the Linux builds on Windows is **not a big deal**; it is however, **unnecessary**, because they include patches that are useless on Windows.

| Singular                                                                                                      | Localized                                                                                                                       | Linux-`DLDXVK`                                                                                                                              | Windows-`DLDXVK`                                                                                                                                  |
|---------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| [`efficiency.pak`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/output/efficiency/efficiency.pak)              | [`efficiency.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/efficiency.pkg.7z)                                    | [`efficiency-linux-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/linux/efficiency-linux-dldxvk.pkg.7z?inline=false)   | [`efficiency-windows-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/windows/efficiency-windows-dldxvk.pkg.7z?inline=false)   |
| [`performance.pak`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/output/performance/performance.pak)           | [`performance.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/performance.pkg.7z)                                  | [`performance-linux-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/linux/performance-linux-dldxvk.pkg.7z?inline=false) | [`performance-windows-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/windows/performance-windows-dldxvk.pkg.7z?inline=false) |


#
</details><!--Downloads-->

<details><!--English::Installation-->
<summary><h2>📥 Installation 📦</h2></summary>

<details><!--English::Installation::Stable-->
<summary><h2>🛡️ ⚙️ Stable 🗜️ 🛡️</h2></summary>

#
</details><!--English::Installation::Stable-->

<details><!--English::Installation::Development-->
<summary><h2>🧮 🧩 Development 🧩 ️🧮</h2></summary>


<details><!--English::Installation::Development::Linux-->
<summary><h2>🧩 🐧 Linux 🐧 ️🧮</h2></summary>

<details><!--English::Installation::Development::Linux::Steam with Proton-->
<summary><h3>♨️ Steam with Proton ⚛️</h3></summary>

#### `0`: Download a build:

File                                                                                                        | Description
------------------------------------------------------------------------------------------------------------|---------------------------------------------------------
[`efficiency.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/efficiency.pkg.7z?inline=false)   | Cleanly balanced optimizations with minimal compromise.
[`performance.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/package/performance.pkg.7z?inline=false) | Full performance optimizations with maximum compromise.

<details><!--English::Installation::Development::Linux::Steam with Proton::Confused?-->
<summary><h4>🤔 Confused? ❔</h4></summary>

> #### ⚠️ Hint! 🛑
>
> Click on the highlighted blue file name to download it.

#
</details><!--English::Installation::Development::Linux::Steam with Proton::Confused?-->

##### `0a`: In your Steam Library, right click `Dying Light`, then click on `Properties	`:

![](resource/linux/en/navigate-to-properties.png)

#

### 🌬️ Final notes 🗒️

#
</details><!--English::Installation::Development::Linux::Steam with Proton-->

#
</details><!--English::Installation::Development::Linux-->

<details><!--English::Installation::Development::Windows-->
<summary><h2>🧩 🪟 Windows 🪟 🧮</h2></summary>

<details><!--English::Installation::Development::Windows::Steam-->
<summary><h3>♨️ Steam ♨️</h3></summary>

### 🌬️ Final notes 🗒️

#
</details><!--English::Installation::Development::Windows::Steam-->

#
</details><!--English::Installation::Development::Windows-->

#
</details><!--English::Installation::Development-->

#
</details><!--English::Installation-->

<details><!--English::Contributing-->
<summary><h2>⚙️ Contributing 📖</h2></summary>

The default documentation is user-facing, if you are a developer who wants to learn more, [see the developer documentation here](https://gitlab.com/dlimi/dlimcsm/-/blob/main/documentation/english/developer_documentation.md) to get started.

#
</details><!--English::Contributing-->

#
</details><!--English-->

<!--КРИТЕРІЇ ЗРУЧНОСТІ РОБОТИ З ДОКУМЕНТАЦІЄЮ-->
<!--1: Не передбачає жодних попередніх знань. Обчислювальної техніки або загалом використання Операційної Системи.-->
<!--2: Використовуйте emoji та Markdown для адекватного викладу та форматування інформації.-->
<!--3: Не використовуйте відступи, якщо це не потрібне для Markdown. Щоб уникнути проблем із форматом, розміром та обмеженням гнізд підрозділів.-->
<!--ПРИМІТКА: Використовуйте https://www.emojicopy.com, щоб отримати Eможи.-->

<details><!--Український-->
<summary><h2>🇺🇦 🇪🇺 🌻 Український 🚅 🇪🇺 🇺🇦</h2></summary>

## 🍃 Dying Light Improved Compatibility Support Module (*`dimcsm`*) 🛠️

Сумісна з кількома гравцями [`dlim`](https://gitlab.com/dlimi/dlim)-заснована на ефективності та продуктивності додаток до [**Dying Light**](https://dl1.dyinglightgame.com).
#

<details><!--Український::Встановлення-->
<summary><h2>📥 Встановлення 📦️</h2></summary>

<details><!--Український::Встановлення::Стабільний-->
<summary><h2>️🛡️ ⚙️ Стабільний 🗜️ 🛡️</h2></summary>

#
</details><!--Український::Встановлення::Стабільний-->

<details><!--Український::Встановлення::Розробка-->
<summary><h2>🧮 🧩 Розробка 🧩 ️🧮</h2></summary>

<details><!--Український::Встановлення::Розробка::Linux/Лінукс-->
<summary><h2>🧩 🐧 Linux/Лінукс 🐧 ️🧮</h2></summary>

<details><!--Український::Встановлення::Розробка::Linux::Steam з Proton-->
<summary><h3>♨️ Steam з Proton ⚛️</h3></summary>

### 🌬️ Підсумкові нотатки 🗒️

#
</details><!--Український::Встановлення::Розробка::Linux/Лінукс::Steam з Proton-->

#
</details><!--Український::Встановлення::Розробка::Linux/Лінукс-->

<details><!--Український::Встановлення::Розробка::Windows-->
<summary><h2>🧩 🪟 Windows 🪟 🧮</h2></summary>

<details><!--Український::Встановлення::Розробка::Windows::Steam-->
<summary><h3>♨️ Steam ♨️</h3></summary>

### 🌬️ Підсумкові нотатки 🗒️

#
</details><!--Український::Встановлення::Розробка::Windows::Steam-->

#
</details>

#
</details><!--Український::Встановлення::Розробка-->

#
</details><!--Український::Встановлення-->

#
</details><!--Український-->

<!--КРИТЕРИИ УДОБСТВА РАБОТЫ С ДОКУМЕНТАЦИЕЙ-->
<!--1: Не предполагает никаких предварительных знаний. вычислительной техники или в целом использования Операционной Системы.-->
<!--2: Используйте еможи и Markdown для адекватного изложения и форматирования информации.-->
<!--3: Не используйте отступы, если это не требуется для Markdown. Чтобы избежать проблем с форматом, размером и ограничением гнезд подразделов.-->
<!--ПРИМЕЧАНИЕ: Используйте https://www.emojicopy.com, чтобы получить Eможи.-->

<details><!--Русский-->
<summary><h2>🇷🇺 🇧🇾 🌨️ Русский 🏔 🇧🇾️ 🇷🇺</h2></summary>

## 🍃 Dying Light Improved Compatibility Support Module (*`dimcsm`*) 🛠️
Совместимая с несколькими игроками [`dlim`](https://gitlab.com/dlimi/dlim)-основанная на эффективности и производительности прибавка к [**Dying Light**](https://dl1.dyinglightgame.com).
#

<details><!--Русский::Установка-->
<summary><h2>📥 Установка 📦</h2></summary>

<details><!--Русский::Установка::Стабильный-->
<summary><h2>🛡️ ⚙️Стабильный 🗜️ 🛡️</h2></summary>

#
</details><!--Русский::Установка::Стабильный-->

<details><!--Русский::Установка::Разработка-->
<summary><h2>🧮 🧩 Разработка 🧩 ️🧮</h2></summary>

<details><!--Русский::Установка::Разработка::Linux-->
<summary><h2>🧩 🐧 Linux/Линукс 🐧 ️🧮</h2></summary>

<details><!--Русский::Установка::Разработка::Linux::Steam с Proton-->
<summary><h3>♨️ Steam с Proton ⚛️</h3></summary>

### 🌬️ Итоговые заметки 🗒️

#
</details><!--Русский::Установка::Разработка::Linux::Steam с Proton-->

#
</details><!--Русский::Установка::Разработка::Linux-->

<details><!--Русский::Установка::Разработка::Windows-->
<summary><h2>🧩 🪟 Windows 🪟 🧮</h2></summary>

<details><!--Русский::Установка::Разработка::Windows::Steam-->
<summary><h3>♨️ Steam ♨️</h3></summary>

### 🌬️ Итоговые заметки 🗒️

#
</details><!--Русский::Установка::Разработка::Windows::Steam-->

#
</details><!--Русский::Установка::Разработка::Windows-->

#
</details><!--Русский::Установка::Разработка-->

#
</details><!--Русский::Установка-->

#
</details><!--Русский-->
