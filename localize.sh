#!/usr/bin/env bash

if [ -f output/performance/performance.pak ]; then
 cp output/performance/performance.pak output/performance/DataBr.pak
 cp output/performance/performance.pak output/performance/DataCn.pak
 cp output/performance/performance.pak output/performance/DataCs.pak
 cp output/performance/performance.pak output/performance/DataDe.pak
 cp output/performance/performance.pak output/performance/DataEl.pak
 cp output/performance/performance.pak output/performance/DataEn.pak
 cp output/performance/performance.pak output/performance/DataEs.pak
 cp output/performance/performance.pak output/performance/DataFr.pak
 cp output/performance/performance.pak output/performance/DataIt.pak
 cp output/performance/performance.pak output/performance/DataJp.pak
 cp output/performance/performance.pak output/performance/DataKo.pak
 cp output/performance/performance.pak output/performance/DataNl.pak
 cp output/performance/performance.pak output/performance/DataPl.pak
 cp output/performance/performance.pak output/performance/DataRu.pak
 cp output/performance/performance.pak output/performance/DataTh.pak
 cp output/performance/performance.pak output/performance/DataTr.pak
 cp output/performance/performance.pak output/performance/DataTw.pak
fi

if [ -f output/efficiency/efficiency.pak ]; then
 cp output/efficiency/efficiency.pak output/efficiency/DataBr.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataCn.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataCs.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataDe.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataEl.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataEn.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataEs.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataFr.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataIt.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataJp.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataKo.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataNl.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataPl.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataRu.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataTh.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataTr.pak
 cp output/efficiency/efficiency.pak output/efficiency/DataTw.pak
fi